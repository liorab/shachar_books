<?php

use Illuminate\Database\Seeder;

class BooksTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('books')->insert([
            [
            'title' => 'book1',
            'author'=>'author1',
            'user_id'=>1,
        ],
        [
            'title' => 'book2',
            'author'=>'author2',
            'user_id'=>1,
        ],
        [
            'title' => 'book3',
            'author'=>'author3',
            'user_id'=>1,
        ],
        [
            'title' => 'book4',
            'author'=>'author4',
            'user_id'=>1,
        ],
        
        ]);

    }
}
