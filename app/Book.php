<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Book extends Model
{
    protected $fillable=
    ['title','status', 'opinion', 'author',
];
    public function user(){
        return $this->hasMany('App\user');

    }

}
