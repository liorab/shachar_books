@extends('layouts.app')
@section('content')
<h1>Your books list</h1>

<a href="{{route('books.favorite', [$opinion=>1])}}">change status</a>

<table>
<tr>
    <th></th>
    <th>Book</th>
    <th>Author</th>
    <th></th>
    <th></th>
</tr>
@foreach($books as $book)
<tr>
    <td> @if ($book->status)
           <input type = 'checkbox' id ="{{$book->id}}" checked>
       @else
           <input type = 'checkbox' id ="{{$book->id}}">
       @endif</td>
    <td color="white">{{$book->title}}</td>
    <td>{{$book->author}}</td>
    <td><a href = "{{route('books.edit', $book->id)}}"> edit </a></td>
    @cannot('user')
    <td><form method='post' action="{{action('BookController@destroy',$book->id)}}">
 
    @csrf
    @method('DELETE')

    <div class = "form-group">
        <input type="submit" class="form-controll" name="submit" value="delete">
    </div>
</form>
</td>
@endcannot
</tr>
@endforeach

</table>
<a href= "{{route('books.create')}}">Create a new book</a>

<script>
       $(document).ready(function(){
           $(":checkbox").click(function(event){
              console.log(event.target.id)
               $.ajax({
                   url: "{{url('books')}}"+'/' + event.target.id,
                   dataType: 'json',
                   type: 'put' ,
                   contentType:'application/json',
                   data:JSON.stringify({'status':event.target.checked, _token:"{{csrf_token()}}"}),
                   processData:false,
                   success: function( data){
                        console.log(JSON.stringify( data ));
                   },
                   error: function(errorThrown ){
                       console.log( errorThrown );
                   }
               });               
           });
       });
   </script>  




@endsection